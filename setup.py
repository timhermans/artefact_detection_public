"""A setuptools based setup module.
See:
https://python-packaging.readthedocs.io/en/latest/minimal.html
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
from os import path
# io.open is needed for projects that support Python 2.7
# It ensures open() defaults to text mode with universal newlines,
# and accepts an argument to specify the text encoding
# Python 3 only projects can skip this import
from io import open

VERSION = '0.1'

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='artefact_detection',  # Required

    version=VERSION,  # Required

    description='Project artefact detection with keras.',  # Optional

    url='https://gitlab.com/timhermans/artefact_detection_public',  # Optional

    author='Tim Hermans',  # Optional

    author_email='tim.hermans@esat.kuleuven.be',  # Optional

    # Note that this is a string of words separated by whitespace, not a list.
    keywords='neonatal signal processing eeg',  # Optional

    packages=find_packages(exclude=['contrib', 'docs', 'tests']),  # Required

    python_requires='>=3.8',

    install_requires=[
    ],  # Optional

    project_urls={  # Optional
        'Source': 'https://gitlab.com/timhermans/artefact_detection_public',
    },
)
