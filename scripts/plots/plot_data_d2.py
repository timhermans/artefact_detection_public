"""
This script plots the EEG of a recording from dataset D2, indicating clean and artefact annotations.
When running this script, make sure the paths to the data folders are set accordingly as described in
the beginning of the code.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import os

import numpy as np
import matplotlib.pyplot as plt

import pandas as pd

from nnsa import EdfReader, EegDataset, NotchIIR, Butterworth

from artefact_detection.utils import get_clean_mask_d2

#%% Set paths.
# Path to the directory containing the edf files of D2
# (downloaded from https://zenodo.org/record/4940267#.Y3YwGXbMJaQ).
edf_data_dir = r'<enter-your-path>'

# Path to the directory containing the txt files with the artefact annotations
# (downloaded from https://github.com/LockyWebb/NeonatalEEGArtefactDetection).
ann_data_dir = r'<enter-your-path>'

# Check whether path was set.
if '<enter-your-path>' in [edf_data_dir, ann_data_dir]:
    raise NotImplementedError('User needs to set the variables `edf_data_dir` and `ann_data_dir`.')

#%% Select EEG to plot.
# EEG number to plot (1-79).
eeg_num = 1

# Define the full filepaths to the edf and annotation files.
fp_edf = os.path.join(edf_data_dir, f'eeg{eeg_num:d}.edf')
fp_ann = os.path.join(ann_data_dir,  f'eeg{eeg_num:d}_summary.txt')

#%% Read raw data.
# Read EEG data using the nnsa package.
with EdfReader(fp_edf) as r:
    eeg_ds = r.read_eeg_dataset(dtype=np.float32)

# Read annotations as DataFrame.
annotations_df = pd.read_csv(fp_ann)

#%% Prepare data.
# Remove '-REF' from channel labels and put in a new EegDataset.
ts_all = eeg_ds.time_series.values()
eeg_ds = EegDataset()
for ts in ts_all:
    ts.label = ts.label.replace('-REF', '').strip()
    ts.label = ts.label.replace('-Ref', '').strip()
    ts.label = ts.label.replace('-ref', '').strip()
    ts.check_label = False
    eeg_ds.append(ts)

# Create EEG montage used for scoring the artefacts.
channel_labels = [
    'Fp1-F3', 'F3-C3', 'C3-P3', 'P3-O1', 'Fp2-F4',
    'F4-C4', 'C4-P4', 'P4-O2', 'Fp1-F7', 'F7-T3', 'T3-T5', 'T5-O1',
    'Fp2-F8', 'F8-T4', 'T4-T6', 'T6-O2', 'Fz-Cz', 'Cz-Pz'
]
channels_1 = []
channels_2 = []
for lab in channel_labels:
    ch1, ch2 = lab.split('-')
    channels_1.append(ch1)
    channels_2.append(ch2)
eeg_ds = eeg_ds.create_bipolar_channels(
    channels_1=channels_1,
    channels_2=channels_2,
    labels=channel_labels,
    missing_mode='error')

# We should have 18 bipolar channels.
assert len(eeg_ds) == 18

#%% Preprocess the EEG.
# Notch filter for power line interference at 50 Hz (in the paper we mistakenly wrote that the Notch freq was 60 Hz).
filt_notch = NotchIIR(f0=50)

# Bandpass Butterworth.
filt = Butterworth(fn=[0.27, 30], order=1)

# Resampling frequency (preferably a power of 2).
fs_res = 128

# Resampling method ('interpolation' or 'polyphase_filtering').
# 'interpolation' is faster and fine if the sampling rate is high enough to avoid aliasing.
# 'polyphase_filtering' may cause problems for large files.
resample_method = 'interpolation'

# Filter and resample with the filters defined above.
eeg_ds = eeg_ds. \
    filtfilt(filt_notch). \
    filter(filt). \
    resample(fs_new=fs_res, method=resample_method)

# Get the clean/artefact mask.
# The mask has shape (n_time, n_channels) and each value has one of three values:
# -1: artefact
#  0: unlabeled
# +1: clean
clean_mask = get_clean_mask_d2(
    df=annotations_df, time=eeg_ds.time,
    channel_labels=channel_labels)

# We can get the array data for the EEG like this:
eeg_array = eeg_ds.asarray(channels_last=True)

# The annotations mask and the eeg array should have the same shapes (n_time, n_channels).
assert clean_mask.shape == eeg_array.shape

# Note: the sampling frequency and channel labels corresponding to the eeg array can be obtained using
# eeg_ds.fs and eeg_ds.channel_labels, respectively.

#%% Plotting.
# Create EEG dataset with only the artefacts (set the rest to nan).
remove_mask = clean_mask != -1
eeg_ds_af = eeg_ds.remove_artefacts_mask(remove_mask.T)  # Mask must have shape (n_channels, n_time).

# Create EEG dataset with only the clean data (set the rest to nan).
remove_mask = clean_mask != 1
eeg_ds_clean = eeg_ds.remove_artefacts_mask(remove_mask.T)  # Mask must have shape (n_channels, n_time).

# Create EEG dataset with only the unlabeled samples (set the rest to nan).
remove_mask = clean_mask != 0
eeg_ds_unlab = eeg_ds.remove_artefacts_mask(remove_mask.T)  # Mask must have shape (n_channels, n_time).

# Init plot.
fig, ax = plt.subplots(tight_layout=True)
plot_kwargs = dict(linewidth=1.2, scale=75)

# Plot artefacts in red.
eeg_ds_af.plot(color='C3', label='Artefact', legend=False, **plot_kwargs)

# Plot clean in green.
eeg_ds_clean.plot(color='C2', label='Clean', legend=False, **plot_kwargs)

# Plot unlabeled in grey.
eeg_ds_unlab.plot(color='C7', alpha=0.75, label='Unlabeled', legend=True, **plot_kwargs)

# Add legend.
plt.legend(loc='upper left', bbox_to_anchor=(1, 1))
plt.title(f'eeg{eeg_num}')
