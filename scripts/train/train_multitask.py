"""
This script demonstrates how to train multi-task model with the architecture presented in the paper.

Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import time
from datetime import timedelta

import numpy as np

from tensorflow import keras
from tensorflow.keras import backend as K

from artefact_detection.keras.architecture import build_MTAEC
from artefact_detection.keras.losses import myCategoricalCrossentropy
from artefact_detection.keras.callbacks import LambdaBetaUpdateCallback, BetaUpdateCallback
from artefact_detection.utils import generate_data

#%% Create dummy data.
x_train, y_train = generate_data(n_segments=100, seed=43)
x_val, y_val = generate_data(n_segments=10, seed=44)
n_channels = x_train.shape[2]

#%% Get network.
model, autoencoder, classifier = build_MTAEC(
    n_channels=n_channels)

# Print a summary of the models.
autoencoder.summary()
classifier.summary()
model.summary()

#%% Training options.
# Beta parameters that scales classifier loss.
beta_final_value = 'auto'  # a float or 'auto'.
beta_steps = 1
beta_wait = 4 if beta_steps == 1 else 0
if beta_final_value == 'auto':
    # Automatically bring both losses to same scale after each epoch based on the ratio of the previous epoch.
    fun = lambda epoch, logs: logs['decoder_loss'] / (logs['classifier_top_loss'] + K.epsilon())
    beta_cb = LambdaBetaUpdateCallback(
        fun=fun, initial_value=1, verbose=1)
elif isinstance(beta_final_value, (int, float)):
    # Start with beta at 0. After waiting 4 epochs (keeping beta at 0), bring beta to its final value in one step/epoch.
    beta_cb = BetaUpdateCallback(
        initial_value=0, final_value=beta_final_value,
        steps=1, wait=4, verbose=1)
else:
    raise ValueError(f'`beta_final_value` must be a number or "auto". Got {beta_final_value}')

# Compile.
optimizer = keras.optimizers.Adam(clipnorm=1.0)
model.compile(
    optimizer=optimizer,
    loss={'decoder': 'mse',
          'classifier_top': myCategoricalCrossentropy()},
    loss_weights={'decoder': 1,
                  'classifier_top': beta_cb.beta},
)

# Collect callbacks.
callbacks = [
    beta_cb,
]

#%% Train.
# Fit the model.
start = time.time()
history = model.fit(x=x_train, y={'decoder': x_train, 'classifier_top': y_train},
                    batch_size=100, epochs=5, validation_data=(x_val, {'decoder': x_val, 'classifier_top': y_val}),
                    callbacks=callbacks)
stop = time.time()
train_time = stop - start
print('Done training! Elapsed time: {}.'.format(str(timedelta(seconds=train_time))))

#%% Predict.
# Classifications.
y_pred = classifier.predict(x_val)
class_pred = np.argmax(y_pred, axis=-1)  # From one-hot-encoding back to class numbers.

# Reconstructions.
x_rec = autoencoder.predict(x_val)  # Shape (n_segments, n_time, n_channels, 1).
