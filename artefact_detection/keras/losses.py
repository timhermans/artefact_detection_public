"""
Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
from tensorflow.keras import backend as K


def myCategoricalCrossentropy(weights=1.0, axis=-1):
    """
    Cross-categorical loss that works for batches with unlabeled examples (where the one-hot encoding is all zeros),
    by rescaling the computation of the mean (ignoring unlabeled examples), such that the loss won't become small due to
    a large number of unlabeled data.

    Adopted from rom https://gist.github.com/wassname/ce364fddfc8a025bfab4348cf5de852d.

    Args:
         weights (tuple): weight for each class, e.g. for 2 class problem (1.0, 10.0).
         axis (int): axis corresponding to the number of classes.

    Examples:
        >>> model.compile(loss=myCategoricalCrossentropy())
    """

    def loss_fn(y_true, y_pred):
        # clip to prevent NaN's and Inf's
        y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())
        # scale predictions so that the class probas of each sample sum to 1
        y_pred /= K.sum(y_pred, axis=axis, keepdims=True)
        # clip to prevent NaN's and Inf's
        y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())
        # calc
        loss = y_true * K.log(y_pred)  # Note that loss for y_true==0 is 0.

        # Apply weights to classes.
        loss = loss * weights

        loss = -K.mean(loss)

        return loss

    return loss_fn
