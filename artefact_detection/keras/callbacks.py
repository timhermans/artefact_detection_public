"""
Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
import numpy as np
from tensorflow import keras
from tensorflow.keras import backend as K


class BetaUpdateCallback(keras.callbacks.Callback):
    """
    Callback to update a non trainable parameter `beta` after each epoch,
    increasing from initial to final value in a predefined number of steps.

    Useful for e.g. applying a warmup strategy, increasing the loss of a regularizer as training progresses.

    Args:
        initial_value (float): initial value of beta.
        final_value (float): final value of beta.
        steps (int): number of epochs in which to increase beta from initial to final value.
        verbose (int): verbose level (if 1, prints the updates to beta).

    Example:
        >>> beta_callback = BetaUpdateCallback(initial_value=0, final_value=1/100, steps=5)
        >>> loss = loss_term_1 + beta_callback.beta * loss_term_2
    """
    def __init__(self, wait=0, initial_value=0.0, final_value=1.0, steps=5, verbose=0):
        super().__init__()
        self.initial_value = float(initial_value)
        self.final_value = float(final_value)
        self.steps = int(steps)
        self.wait = int(wait)
        self.verbose = verbose
        self._beta = K.variable(value=initial_value)
        self._beta._trainable = False

    @property
    def beta(self):
        return self._beta

    @beta.setter
    def beta(self, new_value):
        # Set beta by setting the value in the TF variable.
        K.set_value(self._beta, new_value)

    def on_epoch_begin(self, epoch, logs=None):
        if self.verbose:
            print('On begin epoch {}'.format(epoch))
            print('Beta before update = {}'.format(self.beta))

        # Determine beta based on epoch number.
        f_final = np.clip(((epoch - self.wait) / self.steps), 0, 1)
        new_value = f_final*self.final_value + (1 - f_final)*self.initial_value
        self.beta = new_value

        if self.verbose:
            print('Beta after update = {}'.format(self.beta))


class LambdaBetaUpdateCallback(keras.callbacks.Callback):
    """
    Callback to update a non trainable parameter `beta` at the end of each epoch,
    by executing a function: on_epoch_end(epoch, logs).

    Args:
        fun (function): a function that takes two inputs: epoch and logs (see on_epoch_begin) and returns a float
            (the new value for beta).
        initial_value (float): initial value for beta.
        verbose (int): verbose level (if 1, prints the updates to lambda).

    Example:
        >>> lambda_callback = LambdaBetaUpdateCallback(initial_value=0, fun=lambda epoch, logs: epoch)
    """
    def __init__(self, fun, initial_value=0.0, verbose=0):
        super().__init__()
        self.fun = fun
        self.verbose = verbose
        self._beta = K.variable(value=float(initial_value))
        self._beta._trainable = False

    @property
    def beta(self):
        return self._beta

    @beta.setter
    def beta(self, new_value):
        # Set beta by setting the value in the TF variable.
        K.set_value(self._beta, new_value)

    def on_epoch_end(self, epoch, logs=None):
        if self.verbose:
            print('On end epoch {}'.format(epoch))
            print('Logs:', logs)
            print('Beta before update = {}'.format(self.beta))

        # Determine beta based on function.
        new_value = self.fun(epoch, logs)
        self.beta = new_value

        if self.verbose:
            print('Beta after update = {}'.format(self.beta))
