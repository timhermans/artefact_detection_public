"""
Author: Tim Hermans (tim.hermans@esat.kuleuven.be).
"""
from tensorflow.python.keras import Input
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D, Dropout, Activation, BatchNormalization, \
    SpatialDropout2D, Conv2DTranspose, Reshape, Softmax
from tensorflow.python.keras.models import Model


def build_AE(n_channels):
    """
    Build an autoencoder (AE).

    Args:
        n_channels (int or None): number of channels in the input.
            Set to None to use a variable number of channels.

    Returns:
        autoencoder (keras.Model): AE model.
        encoder (keras.Model): encoder part of the AE model.
        decoder (keras.Model): decoder part of the AE model.
    """
    # Variable segment length and optionally # channels. Possible due to convolutional nature of model.
    input_shape = (None, n_channels, 1)

    # Size of the latent dimension (number of latent feature maps).
    latent_dim = 8

    # Dropout fraction.
    p_dropout = 0.25

    # Activation function.
    activation = 'relu'

    # Include batchnorm layers.
    batchnorm = True

    # Fixed kwargs for conv layer block.
    conv_kwargs = dict(
        # Use same padding to maintain the shape.
        padding='same',
        # No dropout in conv layers.
        dropout=False,
    )

    # Encoder.
    encoder_inputs = Input(shape=input_shape, name="x")
    e0 = encoder_inputs
    e0 = layer_block(Conv2D, filters=16, kernel_size=(3, 1),
                     batchnorm=batchnorm, activation=activation, name='c0', **conv_kwargs)(e0)
    e1 = MaxPooling2D(pool_size=(2, 1), padding='same', name='p1')(e0)
    e1 = layer_block(Conv2D, filters=32, kernel_size=(3, 1),
                     batchnorm=batchnorm, activation=activation, name='c1', **conv_kwargs)(e1)
    e2 = MaxPooling2D(pool_size=(2, 1), padding='same', name='p2')(e1)
    e2 = layer_block(Conv2D, filters=64, kernel_size=(3, 1),
                     batchnorm=batchnorm, activation=activation, name='c2', **conv_kwargs)(e2)
    e3 = MaxPooling2D(pool_size=(2, 1), padding='same', name='p3')(e2)
    e3 = layer_block(Conv2D, filters=64, kernel_size=(3, 1), name='c3',
                     batchnorm=batchnorm, activation=activation, **conv_kwargs)(e3)
    e4 = MaxPooling2D(pool_size=(2, 1), padding='same', name='p4')(e3)
    e4 = layer_block(Conv2D, filters=64, kernel_size=(3, 1), name='c4',
                     batchnorm=batchnorm, activation=activation, **conv_kwargs)(e4)
    e5 = MaxPooling2D(pool_size=(2, 1), padding='same', name='p5')(e4)
    e5 = layer_block(Conv2D, filters=64, kernel_size=(3, 1), name='c5',
                     batchnorm=batchnorm, activation=activation, **conv_kwargs)(e5)
    e6 = MaxPooling2D(pool_size=(2, 1), padding='same', name='p6')(e5)
    e6 = layer_block(Conv2D, filters=64, kernel_size=(3, 1), name='c6',
                     batchnorm=batchnorm, activation=activation, **conv_kwargs)(e6)
    e7 = MaxPooling2D(pool_size=(2, 1), padding='same', name='p7')(e6)

    if p_dropout > 0:
        e7 = SpatialDropout2D(rate=p_dropout)(e7)

    # Convolutional layer to add temporal context.
    k_size = 15  # Use a larger size for more temporal context.
    e8 = layer_block(Conv2D, filters=64, kernel_size=(k_size, 1), name='c8',
                     batchnorm=batchnorm, activation=activation, **dict(conv_kwargs))(e7)

    # Reduce all deep features to the desired latent dim.
    e9 = layer_block(Conv2D, filters=latent_dim * 2, kernel_size=(1, 1), name='c9',
                     batchnorm=batchnorm, activation=activation, **dict(conv_kwargs))(e8)
    z = layer_block(Conv2D, filters=latent_dim, kernel_size=(1, 1), name='cz',
                    batchnorm=False, activation=None, **dict(conv_kwargs))(e9)

    # Use sigmoid activation function for latent dim to squeeze values between 0 and 1.
    z = Activation('sigmoid', name='z')(z)

    # Decoder.
    latent_inputs = Input(shape=z.shape[1:], name='latent_inputs')
    d8 = layer_block(Conv2DTranspose, filters=latent_dim * 2, strides=(1, 1), kernel_size=(1, 1),
                     batchnorm=batchnorm, activation=activation, name='d8',
                     **dict(conv_kwargs))(latent_inputs)
    d7 = layer_block(Conv2DTranspose, filters=64, strides=(1, 1), kernel_size=(k_size, 1),
                     batchnorm=batchnorm, activation=activation, name='d7',
                     **dict(conv_kwargs))(d8)
    if p_dropout > 0:
        d7 = SpatialDropout2D(rate=p_dropout)(d7)

    d6 = layer_block(Conv2DTranspose, filters=64, strides=(2, 1), kernel_size=(3, 1), name='d6',
                     batchnorm=batchnorm, activation=activation, **dict(conv_kwargs))(d7)
    d5 = layer_block(Conv2DTranspose, filters=64, strides=(2, 1), kernel_size=(3, 1), name='d5',
                     batchnorm=batchnorm, activation=activation, **dict(conv_kwargs))(d6)
    d4 = layer_block(Conv2DTranspose, filters=64, strides=(2, 1), kernel_size=(3, 1), name='d4',
                     batchnorm=batchnorm, activation=activation, **dict(conv_kwargs))(d5)
    d3 = layer_block(Conv2DTranspose, filters=64, strides=(2, 1), kernel_size=(3, 1),
                     batchnorm=batchnorm, activation=activation, name='d3', **conv_kwargs)(d4)
    d2 = layer_block(Conv2DTranspose, filters=64, strides=(2, 1), kernel_size=(3, 1),
                     batchnorm=batchnorm, activation=activation, name='d2', **conv_kwargs)(d3)
    d1 = layer_block(Conv2DTranspose, filters=32, strides=(2, 1), kernel_size=(3, 1),
                     batchnorm=batchnorm, activation=activation, name='d1', **conv_kwargs)(d2)
    d0 = layer_block(Conv2DTranspose, filters=16, strides=(2, 1), kernel_size=(3, 1),
                     batchnorm=batchnorm, activation=activation, name='d0', **conv_kwargs)(d1)

    # Reconstruction.
    xr = layer_block(Conv2DTranspose, filters=1, strides=(1, 1), kernel_size=(3, 1),
                     batchnorm=False, activation=None, **conv_kwargs, name='decoder_out')(d0)
    decoder_outputs = xr

    # Check if input and output shapes match.
    _check_ae_input_output(encoder_inputs, decoder_outputs)

    # Create models.
    encoder = Model(encoder_inputs, z, name="encoder")
    decoder = Model(latent_inputs, decoder_outputs, name="decoder")
    autoencoder = Model(encoder_inputs, decoder(encoder(encoder_inputs)), name='AE')

    return autoencoder, encoder, decoder


def build_classifier_top(input_shape):
    """
    Helper function to build the classifier that can be put on top of an encoder.

    Args:
        input_shape (tuple): shape of the input (time, channels, features).
            The first dimension is expected to correspond to the number of channels.

    Returns:
        classifier: keras model.
    """
    # Settings.
    conv_kwargs = dict(dropout=False)
    batchnorm = True
    activation = 'relu'
    n_channels = input_shape[1]
    n_classes = 2

    if n_channels is None:
        # Single channel mode.
        n_channels = 1

    # Input layer (latent layer).
    z = Input(input_shape, name='classifier_input')
    latent_dim = z.shape[-1]

    # Some conv layers exploiting spatial information.
    x = layer_block(Conv2D, filters=int(latent_dim * n_channels), kernel_size=(1, n_channels),
                    batchnorm=batchnorm, activation=activation, **dict(conv_kwargs, padding='valid'))(z)
    x = layer_block(Conv2D, filters=int(latent_dim * n_channels), kernel_size=(1, 1),
                    batchnorm=batchnorm, activation=activation, **dict(conv_kwargs))(x)

    # Reshape back to have one feature vector per channel
    # (unless the channel dimension is 1, in that case no reshape is needed).
    if n_channels != 1:
        x = Reshape([-1, n_channels, latent_dim])(x)

    # Classification layer and softmax.
    x = layer_block(Conv2D, filters=n_classes, kernel_size=(1, 1),
                    batchnorm=False, activation=None, **dict(conv_kwargs))(x)
    classifier_output = Softmax(axis=-1, name='classifier_out')(x)

    classifier_top = Model(z, classifier_output, name="classifier_top")

    return classifier_top


def build_MTAEC(n_channels):
    """
    Build multi-task autoencoder and classifier.

    Args:
        n_channels (int or None): number of channels in the input.
        Set to None to use a variable number of channels (but then spatial information won't be exploited).
    """
    # Build autoencoder.
    _, encoder, decoder = build_AE(n_channels=n_channels)

    # Input layer.
    encoder_input = Input(shape=encoder.input_shape[1:], name="inputs")

    # Get encoder output.
    z = encoder(encoder_input)
    latent_shape = encoder.output_shape[1:]

    # Build classifier top (z -> y).
    classifier_top = build_classifier_top(input_shape=latent_shape)

    # Build autoencoder (x -> xr).
    autoencoder = Model(encoder_input, decoder(z), name='MTAE')

    # Build classifier (x -> y).
    classifier = Model(encoder_input, classifier_top(z), name='MTC')

    # Build multitask (multioutput) model.
    model = Model(encoder_input, outputs=[decoder(z), classifier_top(z)], name='MTAEC')

    return model, autoencoder, classifier


def layer_block(layer, *args,
                batchnorm=True, batchnorm_kwargs=None,
                activation='relu', activation_kwargs=None,
                dropout=False, dropout_kwargs=None, **kwargs):
    """
    Layer with optionally added batch normalization, activation and dropout regularization.

    Args:
        layer (keras.Layer): keras layer object. E.g., layer=Conv2D
        *args: optional positional arguments for the keras layer.
        batchnorm (bool): bool specifying whether to add a BatchNormalization layer.
        batchnorm_kwargs (dict): optional keyword arguments for BatchNormalization().
        activation (str or None or Activation): specifies the activation used. See Activation().
        activation_kwargs (dict): optional keyword arguments for Activation().
        dropout (bool): bool specifying whether to add a Dropout layer.
        dropout_kwargs (dict): optional keyword arguments for Dropout().
        **kwargs: optional keyword arguments for `layer`.

    Returns:
        block (function): function that takes in a keras layer and returns the
            output layer after passing the input thtough the specified `layer`
            and BatchNormalization, Activation and Dropout layers.
    """
    def block(x):
        # Layer.
        x = layer(*args, **kwargs)(x)

        # Add batch normalization layer.
        if batchnorm:
            x = BatchNormalization(**batchnorm_kwargs if batchnorm_kwargs is not None else dict())(x)

        # Add activation layer.
        if activation is not None:
            x = Activation(activation, **activation_kwargs if activation_kwargs is not None else dict())(x)

        # Add dropout layer.
        if dropout:
            x = Dropout(**dropout_kwargs if dropout_kwargs is not None else dict())(x)

        return x

    return block


def _check_ae_input_output(encoder_inputs, decoder_outputs):
    """
    Check whether the input and output shape of an AE match. Raise a ValueError error if not.
    """
    if encoder_inputs.shape[1] is not None and decoder_outputs.shape[1] is not None:
        # Only if input is not variable size.
        if tuple(encoder_inputs.shape) != tuple(decoder_outputs.shape):
            raise ValueError('AE network not compatible for given input shape. Got input shape {} and output shape {}. '
                             'Try a different input shape (powers of 2 generally work well).'
                             .format(tuple(encoder_inputs.shape), tuple(decoder_outputs.shape)))
