import numpy as np


def generate_data(n_segments, segment_length=30, n_channels=8, seed=None):
    """
    Function to generate random data for code-testing purposes.

    Args:
        n_segments (int): number of segments (samples) generate.
        segment_length (int): length of a segment (in seconds).
        n_channels (int): number of channels.
        seed (int, None): random seed.

    Returns:
        x (np.ndarray): random data representing EEG data (fs=128 Hz) with shape (n_segments, n_time, n_channels, 1).
        y (np.ndarray): random data representing classification output for the EEG
            (fs=1 Hz, 1 for clean, 0 for artefact) with shape (n_segments, n_time, n_channels, n_classes=2).
    """
    rng = np.random.RandomState(seed=seed)

    # Sampling frequencies of the input (eeg) and output (mask).
    fs_eeg = 128
    fs_mask = 1

    # Simulate EEG data (fs=128 Hz). Shape (n_segments, n_time, n_channels, 1).
    x = rng.normal(loc=0, scale=1, size=(n_segments, int(segment_length*fs_eeg), n_channels, 1))

    # Simulate clean masks (fs=1 Hz, 1 for clean, 0 for artefact). Shape (n_segments, n_time, n_channels).
    clean_mask = (rng.random((n_segments, int(segment_length*fs_mask), n_channels)) > 0.5).astype(float)

    # To one-hot-encoding.
    y_artefact = clean_mask == 0
    y_clean = clean_mask == 1
    y = np.stack((y_artefact, y_clean), axis=3).astype(np.float32)

    return x, y


def get_clean_mask_d2(df, time, channel_labels):
    """
    Return a mask for `time` and `channel_labels` that has one of three values:
        -1: artefact
         0: unlabeled
        +1: clean

    Args:
        df (pd.DataFrame): df with annotations as they appear in the .text files of dataset D2
            (columns names should be the channel labels, Time_secs, Duration_secs).
        time (np.ndarray): time array with time in seconds for which to get annotations.
        channel_labels (list): list with channel labels for which to compute the mask.

    Returns:
        mask (np.ndarray): array with shape (len(time), len(channel_labels)).

    """
    # Init mask with zeros (unlabeled).
    mask = np.zeros((len(time), len(channel_labels)))

    # Loop over annotations in df.
    for _, row in df.iterrows():
        # Map for annotations:
        # 1: Clean
        # 2: Device interference
        # 3: EMG
        # 4: Movement
        # 5: Electrode
        # 6: Other
        # 7: Biological rhythm

        # Label 1 is clean, zeros are unlabeled.
        channel_mask = row[channel_labels].values

        # Labels higher than 1 are artefacts.
        channel_mask[channel_mask > 1] = -1

        # Read the start and stop time and determine the start and stop index (use the time array).
        start = row['Time_secs']
        duration = row['Duration_secs']
        stop = start + duration
        if duration > 1:
            assert np.any(time >= start)
        idx_start = np.argmax(time >= start)  # Returns first index where time >= start.
        idx_stop = np.argmax(time >= stop) if np.any(time >= stop) else len(time)

        # Insert the values in the mask.
        mask[idx_start: idx_stop, :] = channel_mask[np.newaxis, :]

    mask = mask.astype(int)

    return mask
