# Artefact detection for neonatal EEG using a multi-task convolutional neural network.

## About
This repository contains code for training and evaluating neural networks for artefact detection in neonatal EEG using a multi-task convolutional neural network.
This repository supports the following paper:  
* T. Hermans et al.,  “A multi-task and multi-channel convolutional neural network for semi-supervised neonatal artefact detection,”
  Journal of Neural Engineering, vol. 20, no. 2, p. 26013, Mar. 2023, doi: 10.1088/1741-2552/acbc4b.
  https://pubmed.ncbi.nlm.nih.gov/36791462/

## Dependencies
* python=>3.8
* [nnsa](https://gitlab.com/timhermans/nnsa_public)  
Note: the nnsa package automatically installs tensorflow 2, which is the deep learning library.

## Installation
1. Clone all required packages to your local machine. 
 
    To do this using the command line, open a git bash command window and run the following commands:
    ```
    cd <directory-of-your-choice>
    git clone https://gitlab.com/timhermans/nnsa_public.git
    git clone https://gitlab.com/timhermans/artefact_detection_public.git
    ```
    
2. Install the cloned packages into an environment with python 3.8 already installed (e.g. using (ana)conda), see also [here](https://gitlab.com/timhermans/nnsa_public) :
    ```
    pip install ./nnsa_public
    pip install ./artefact_detection_public
    ```

3. Open an IDE (e.g. spyder or PyCharm) and test if installation was successful by running a script in /scripts
    
    Make sure that the IDE is using the newly create conda environment.
    
## Using the code
### Training the model.
The *artefact_detection/keras* directory contains code with a keras implementation of the neural network and loss function.
The script *scripts/train/train_multitask.py* contains an example code to train the multitask model.

### Apply trained model
The class that implements the artefact detection model is the CleanDetectorCnn in the nnsa package (see nnsa/artefacts/clean_detector_cnn.py).
Note that conceptually, this model is regarded as a detector of clean EEG, so the positive class corresponds to clean, as explained in the documentation of the class and its functions.

Example scripts that demonstrate how to apply the trained model to new data can be found in the *scripts/apply_model* directory:
* *apply_to_array.py:* demonstrates how to apply the trained model to new EEG, with EEG in array format.
* *apply_to_eegdataset.py:* demonstrates how to apply the trained model to new EEG, with EEG in EegDataset (an nnsa object) format.

### Plotting EEG and annotations of D2
The script *scripts/plots/plot_data_d2.py* plots the EEG of a recording from dataset D2, indicating clean and artefact annotations.
When running this script, make sure the paths to the data folders are set accordingly as described in the beginning of the code.

## Citation
Please cite the following paper when using this code or the models in this repository:
* T. Hermans et al.,  “A multi-task and multi-channel convolutional neural network for semi-supervised neonatal artefact detection,”
  Journal of Neural Engineering, vol. 20, no. 2, p. 26013, Mar. 2023, doi: 10.1088/1741-2552/acbc4b.
  https://pubmed.ncbi.nlm.nih.gov/36791462/
  
## Contact
For questions, contact Tim Hermans: tim-hermans@hotmail.com
